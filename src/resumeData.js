let resumeData = {
    "imagebaseurl":"https://rbhatia46.github.io/",
    "name": "Zhanara Kolbaeva",
    "role": "DevOps trainee at Archi's Academy",
    "linkedinId":"Your LinkedIn Id",
    "skypeid": "Your skypeid",
    "roleDescription": "I like ...",
    "socialLinks":[
        {
          "name":"linkedin",
          "url":"https://www.linkedin.com/in/zhanara-kolbaeva",
          "className":"fa fa-linkedin"
        },
        {
          "name":"github",
          "url":"http://github.com/zkolbaeva",
          "className":"fa fa-github"
        },
        {
          "name":"skype",
          "url":"http://twitter.com",
          "className":"fa fa-twitter"
        }
      ],
    "aboutme":"I am currently a final year student at Eskisehir Osmangazi University.",
    "address":"Turkey",
    "website":"https://rbhatia46.github.io",
    "education":[
      {
        "UniversityName":"Eskisehir Osmangazi University",
        "specialization":"Computer Engineering",
        "MonthOfPassing":"Sep",
        "YearOfPassing":"2021",
        "Achievements":"Some Achievements"
      },
      {
        "UniversityName":"Some University",
        "specialization":"Some specialization",
        "MonthOfPassing":"Jan",
        "YearOfPassing":"2018",
        "Achievements":"Some Achievements"
      }
    ],
    "work":[
      {
        "CompanyName":"Habitat Academy",
        "specialization":"English Language Teacher",
        "MonthOfLeaving":"Present",
        "YearOfLeaving":"Present",
        "Achievements":"Some Achievements"
      },
      {
        "CompanyName":"Some Company",
        "specialization":"Some specialization",
        "MonthOfLeaving":"Jan",
        "YearOfLeaving":"2018",
        "Achievements":"Some Achievements"
      }
    ],
    "skillsDescription":"Your skills here",
    "skills":[
      {
        "skillname":"Docker"
      },
      {
        "skillname":"Linux"
      },
      {
        "skillname":"Git"
      }
    ],
    "portfolio":[
      {
        "name":"project1",
        "description":"mobileapp",
        "imgurl":"images/portfolio/phone.jpg"
      },
      {
        "name":"project2",
        "description":"mobileapp",
        "imgurl":"images/portfolio/project.jpg"
      },
      {
        "name":"project3",
        "description":"mobileapp",  
        "imgurl":"images/portfolio/project2.png"
      },
      {
        "name":"project4",
        "description":"mobileapp",
        "imgurl":"images/portfolio/phone.jpg"
      }
    ],
    "testimonials":[
      {
        "description":"This is a sample testimonial",
        "name":"Some technical guy"
      },
      {
        "description":"This is a sample testimonial",
        "name":"Some technical guy (some changes for committing)"
      }
    ]
  }
  
  export default resumeData