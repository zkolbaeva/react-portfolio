FROM node:alpine
WORKDIR /opt
COPY . .
EXPOSE 3000
CMD ["npm", "start"]
